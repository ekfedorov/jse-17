package ru.ekfedorov.tm.command;

import ru.ekfedorov.tm.api.service.ServiceLocator;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute() throws Exception;

    @Override
    public String toString() {
        String result = "";
        if (!isEmpty(name())) result += name();
        if (!isEmpty(arg())) result += " [" + arg() + "]";
        if (!isEmpty(description())) result += " - " + description();
        return result;
    }

}
